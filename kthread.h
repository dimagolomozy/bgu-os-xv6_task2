#ifndef KTHREAD_H
#define KTHREAD_H

#define MAX_STACK_SIZE 4000
#define MAX_MUTEXES 64
#define MAX_MUTEX_QUEUE NTHREAD*NPROC

struct kthread;


/********************************
        The API of the KLT package
 ********************************/

int kthread_create(void*(*start_func)(), void* stack, uint stack_size);
int kthread_id(void);
void kthread_exit(void);
int kthread_join(int thread_id);

enum mutexstate { MUTEX_UNUSED, MUTEX_USED };

typedef struct kthread_mutex {
	int id;
	uint locked;
	enum mutexstate state;
	struct kthread* queue[MAX_MUTEX_QUEUE];
	int queueHead;
	int queueTail;
} kthread_mutex_t;

int kthread_mutex_alloc(void);
int kthread_mutex_dealloc(int mutex_id);
int kthread_mutex_lock(int mutex_id);
int kthread_mutex_unlock(int mutex_id);
int kthread_mutex_yieldlock(int mutex_id1, int mutex_id2);

#endif /* KTHREAD_H */



