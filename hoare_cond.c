#include "types.h"
#include "stat.h"
#include "user.h"
#include "hoare_cond.h"

static int condID = 1;

hoare_cond_t* hoare_cond_alloc()
{
	hoare_cond_t *hcond;

	hcond = (hoare_cond_t*)malloc(sizeof(*hcond));
	if (hcond == 0)
		return 0;

	hcond->csMutex = kthread_mutex_alloc();
	hcond->queueMutex = kthread_mutex_alloc();
	hcond->count = 0;
	hcond->id = condID++;

	if ( (hcond->csMutex < 0) || (hcond->queueMutex < 0) )
		return 0;

	// lock the queue mutex so kthreads will wait in the queue
	if (kthread_mutex_lock(hcond->queueMutex) < 0)
		return 0;

	return hcond;
}

int hoare_cond_dealloc(hoare_cond_t *hcond)
{
	int chk = 0;

	if (hcond->count != 0)
	{
		printf(1, "try to dealloc hoare cond with threads still in the queue\n");
		return -1;
	}

	// release the lock. (should be no waiting)
	chk += kthread_mutex_unlock(hcond->queueMutex);

	// dealloc queue Mutex
	chk += kthread_mutex_dealloc(hcond->queueMutex);

	// dealloc cs Mutex
	chk += kthread_mutex_dealloc(hcond->csMutex);

	// free hoare cond
	free(hcond);

	if (chk < 0)
	{
		printf(1, "Error deallocating hesa cond: #%d\n", hcond->id);
		return -1;
	}

	return 0;
}

/**
 * Must enter this function with mutex_id locked
 */
int hoare_cond_wait(hoare_cond_t *hcond, int mutex_id)
{
	// enter CS
	kthread_mutex_lock(hcond->csMutex);

	// Release mutex_id
	if (kthread_mutex_unlock(mutex_id) < 0)
	{
		printf(1, "mutex_id: #%d is not locked\n", mutex_id);
		kthread_mutex_unlock(hcond->csMutex);
		return -1;
	}

	// add 1 to waiting kthread counter
	hcond->count++;

	// exit CS
	kthread_mutex_unlock(hcond->csMutex);

	// put kthread in conds queue
	// will block the waiting kthread and call sched()
	kthread_mutex_lock(hcond->queueMutex);
	// ... will come back with mutex_id locked from the signaler kthread.

	/** woke up: **/

	// acquire mutex back
	//kthread_mutex_lock(mutex_id);

	return 0;
}

/**
 *	This function must be called with mutex_id locked.
 *	The function passed the mutex_id lock to the signaled thread.
 *	It's the signaled thread job to release the lock on mutex_id
 *	until then, the caller will be blocked.
 */
int hoare_cond_signal(hoare_cond_t *hcond, int mutex_id)
{
	// enter CS
	kthread_mutex_lock(hcond->csMutex);

	// if we have waiting kthreads
	if (hcond->count != 0)
	{
		hcond->count--;
		// passes mutex_id lock to the waiting thread
		// the waiting thread will get this lock and queueMutex will stay locked
		if (kthread_mutex_yieldlock(mutex_id, hcond->queueMutex) < 0)
		{
			printf(1, "failed to yieldlock from mutex: #%d to mutex: #%d\n", mutex_id, hcond->queueMutex);
			hcond->count++;
			kthread_mutex_unlock(hcond->csMutex);
			return -1;
		}

		// exit CS
		kthread_mutex_unlock(hcond->csMutex);

		// lock back on mutex_id
		// its the signaled thread job to release it first
		kthread_mutex_lock(mutex_id);

		return 0;
	}

	// exit CS
	kthread_mutex_unlock(hcond->csMutex);

	return 0;
}

int hoare_cond_waitingCount(hoare_cond_t *hcond)
{
	return hcond->count;
}








