#include "types.h"
#include "stat.h"
#include "user.h"
#include "mesa_cond.h"
#include "hoare_cond.h"
#include "mesa_slots_monitor.h"
#include "hoare_slots_monitor.h"

int MAX_STACK_SIZE = 1024;

void damn_start_func()
{
	int i=0;
	printf(1, "\ntid: %d running\n", kthread_id());
	while(i < 100) { i++; }
	sleep(500);
	printf(1, "\ntid: %d done\n", kthread_id());
	kthread_exit();
}

void kthread_Test()
{
	int *mem;
	int i, tids[16];
	int threadAmount = 3;

	for (i = 0; i< threadAmount; i++)
	{
		mem = malloc(MAX_STACK_SIZE);
		memset(mem, 0, sizeof(*mem));
		tids[i] = kthread_create(&damn_start_func, mem, MAX_STACK_SIZE);
	}
	//sleep(100);
	for (i = 0; i< threadAmount; i++)
	{
		kthread_join(tids[i]);
	}

	printf(1, "main thread is done\n");
}

void another_test()
{

	printf(1, "tid: %d running\n", kthread_id());
	kthread_mutex_lock(0);
	printf(1, "tid: %d locked on mutex 0\n", kthread_id());
	sleep(100);
	kthread_mutex_unlock(0);
	printf(1, "tid: %d exits\n", kthread_id());
	kthread_exit();
}

void mutex_Test()
{
	int mutex, tid[2], *mem1, *mem2;

	mem1 = malloc(MAX_STACK_SIZE);
	memset(mem1, 0, sizeof(*mem1));
	mem2 = malloc(MAX_STACK_SIZE);
	memset(mem2, 0, sizeof(*mem2));

	printf(1, "tid: %d running\n", kthread_id());
	mutex = kthread_mutex_alloc();
	kthread_mutex_lock(mutex);
	printf(1, "tid: %d locked on mutex 0\n", kthread_id());
	tid[0] = kthread_create((void*)another_test, mem1, MAX_STACK_SIZE);
	tid[1] = kthread_create((void*)another_test, mem2, MAX_STACK_SIZE);
	printf(1, "tid: %d going to sleep 500 \n", kthread_id());
	sleep(500);
	printf(1, "tid: %d woke up\n", kthread_id());
	kthread_mutex_unlock(mutex);
	printf(1, "tid: %d unlocks mutex 0\n", kthread_id());
	kthread_join(tid[0]);
	kthread_join(tid[1]);
	printf(1, "tid: %d exits\n", kthread_id());

}

int mutex;
int mutex2;
mesa_cond_t *mcond;
hoare_cond_t *hcond;

void mesa_test1()
{
	kthread_mutex_lock(mutex);
	printf(1, "tid: %d locked on mutex: %d\n", kthread_id(), mutex);
	printf(1, "tid: %d going to wait on cond\n", kthread_id());
	mesa_cond_wait(mcond, mutex);
	printf(1, "tid: %d woke up with lock %d\n", kthread_id(), mutex);
	kthread_exit();
}

void mesa_test2()
{
	sleep(100);
	printf(1, "tid: %d is running\n", kthread_id());
	kthread_mutex_lock(mutex);
	printf(1, "tid: %d locked on mutex: %d\n", kthread_id(), mutex);
	mesa_cond_signal(mcond);
	printf(1, "tid: %d signaled cond\n", kthread_id());
	kthread_mutex_unlock(mutex);
	printf(1, "tid: %d unlocked mutex %d\n", kthread_id(), mutex);
	kthread_exit();
}

void mesa_cond_Test()
{
	int tid[2], *mem1, *mem2;

	mutex = kthread_mutex_alloc();
	mcond = mesa_cond_alloc();

	mem1 = malloc(MAX_STACK_SIZE);
	memset(mem1, 0, sizeof(*mem1));
	mem2 = malloc(MAX_STACK_SIZE);
	memset(mem2, 0, sizeof(*mem2));

	tid[0] = kthread_create((void*)mesa_test1, mem1, MAX_STACK_SIZE);
	tid[1] = kthread_create((void*)mesa_test2, mem2, MAX_STACK_SIZE);

	kthread_join(tid[0]);
	kthread_join(tid[1]);

	mesa_cond_dealloc(mcond);
	printf(1, "tid: %d exits\n", kthread_id());
}

void hoare_test1()
{
	kthread_mutex_lock(mutex);
	printf(1, "tid: %d locked on mutex: %d\n", kthread_id(), mutex);
	printf(1, "tid: %d going to wait on cond\n", kthread_id());
	hoare_cond_wait(hcond, mutex);
	printf(1, "tid: %d woke up with lock %d\n", kthread_id(), mutex);
	kthread_mutex_unlock(mutex2);
	printf(1, "tid: %d unlocked mutex %d\n", kthread_id(), mutex2);
	kthread_mutex_unlock(mutex);
	printf(1, "tid: %d unlocked mutex %d\n", kthread_id(), mutex);
	kthread_exit();
}

void hoare_test2()
{
	sleep(100);
	printf(1, "tid: %d is running\n", kthread_id());
	kthread_mutex_lock(mutex2);
	printf(1, "tid: %d locked on mutex: %d\n", kthread_id(), mutex2);
	hoare_cond_signal(hcond, mutex2);
	printf(1, "tid: %d signaled cond with mutex: %d\n", kthread_id(), mutex2);
	//kthread_mutex_unlock(mutex);
	//printf(1, "tid: %d unlocked mutex %d\n", kthread_id(), mutex);
	kthread_exit();
}

void hoare_cond_Test()
{
	int tid[2], *mem1, *mem2;

	mutex = kthread_mutex_alloc();
	mutex2 = kthread_mutex_alloc();
	hcond = hoare_cond_alloc();
	mem1 = malloc(MAX_STACK_SIZE);
	memset(mem1, 0, sizeof(*mem1));
	mem2 = malloc(MAX_STACK_SIZE);
	memset(mem2, 0, sizeof(*mem2));

	tid[0] = kthread_create((void*)hoare_test1, mem1, MAX_STACK_SIZE);
	tid[1] = kthread_create((void*)hoare_test2, mem2, MAX_STACK_SIZE);

	kthread_join(tid[0]);
	kthread_join(tid[1]);

	hoare_cond_dealloc(hcond);
	printf(1, "tid: %d exits\n", kthread_id());
}


int
main(int argc, char *argv[])
{
	/* DO NOT ENABLE ALL TEST TOGETHER */
	/* DO NOT ENABLE ALL TEST TOGETHER */

//	int m = kthread_mutex_alloc();
//	printf(1, "lock mutext. return %d\n",kthread_mutex_lock(m));
//	printf(1, "unlock mutext. return %d\n",kthread_mutex_unlock(m));
//	printf(1, "unlock mutext. return %d\n",kthread_mutex_unlock(m));
	if (fork() == 0)
	{
		kthread_Test();
	}
	wait();
	//mutex_Test();
	//mesa_cond_Test();
	//hoare_cond_Test();
	exit();
}

