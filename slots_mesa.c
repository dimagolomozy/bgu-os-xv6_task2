#include "types.h"
#include "stat.h"
#include "user.h"
#include "mesa_slots_monitor.h"

mesa_slots_monitor_t *monitor;
int n;

void teacher()
{
	sleep(400);
	// test exec
//	char *s = "ls";
//	char **a = malloc(sizeof(char));
//	exec(s, a);
	printf(1, "teacher #%d is up\n", kthread_id());
	mesa_slots_monitor_addslots(monitor, n);
	printf(1, "teacher #%d exits\n", kthread_id());
	kthread_exit();
}

void student()
{
	printf(1, "student: #%d is up\n", kthread_id());
	mesa_slots_monitor_takeslot(monitor);
	printf(1, "student: #%d bye bye \n", kthread_id());

	kthread_exit();
}

/**
 * Main function. run like this:
 * > slots_mesa m n
 * m - [number of students]
 * n - [the number of slots to publish]
 */
int main(int argc, char *argv[])
{
  int *tid, **mem;
  int m, i;
  int stack_size = 1024;

  if(argc != 3)
  {
    printf(1, "Error in args\n");
    exit();
  }

  m = atoi(argv[1]) + 1;
  n = atoi(argv[2]);
  monitor = mesa_slots_monitor_alloc();

  tid = malloc(sizeof(int) * m);
  mem = malloc(sizeof(int*) * m);

  for (i = 0; i < m; i++)
  {
	  mem[i] = (int*)malloc(stack_size);
	  memset(mem[i], 0, sizeof(*mem[i]));
  }

  // create teacher
  tid[0] = kthread_create((void*)teacher, mem[0], stack_size);
  // create studnets
  for (i = 1; i < m; i++)
  {
	  tid[i] = kthread_create((void*)student, mem[i], stack_size);
  }

  // join all students
  for (i = 1; i < m; i++)
  {
	  kthread_join(tid[i]);
  }

  // send stop adding to teacher
  mesa_slots_monitor_stopadding(monitor);

  // join teacher
  kthread_join(tid[0]);

  mesa_slots_monitor_dealloc(monitor);

  // free all
  for (i = 0; i < m; i++)
	  free(mem[i]);
  free(mem);
  free(tid);

  exit();
}

