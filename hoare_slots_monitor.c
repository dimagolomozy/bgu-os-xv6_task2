#include "types.h"
#include "stat.h"
#include "user.h"
#include "hoare_slots_monitor.h"

static int monitorID = 1;

hoare_slots_monitor_t* hoare_slots_monitor_alloc()
{
	hoare_slots_monitor_t *hmonitor = (hoare_slots_monitor_t*)malloc(sizeof(*hmonitor));
	if (hmonitor == 0)
		return 0;

	hmonitor->slotsHcond = hoare_cond_alloc();
	if (hmonitor->slotsHcond == 0)
		return 0;

	hmonitor->addingHcond = hoare_cond_alloc();
	if (hmonitor->addingHcond == 0)
		return 0;

	hmonitor->addingMutex = kthread_mutex_alloc();
	hmonitor->slotsMutex = kthread_mutex_alloc();
	if ( (hmonitor->addingMutex < 0) || (hmonitor->slotsMutex < 0) )
		return 0;

	hmonitor->slots = 0;
	hmonitor->stopAdding = 0;
	hmonitor->id = monitorID++;

	return hmonitor;
}

int hoare_slots_monitor_dealloc(hoare_slots_monitor_t *hmonitor)
{
	int chk = 0;

	// dealloc slots cond
	chk += hoare_cond_dealloc(hmonitor->slotsHcond);

	// dealloc adding cond
	chk += hoare_cond_dealloc(hmonitor->addingHcond);

	// dealloc slots mutex
	chk += kthread_mutex_dealloc(hmonitor->slotsMutex);

	// dealloc adding mutex
	chk += kthread_mutex_dealloc(hmonitor->addingMutex);

	free(hmonitor);

	if (chk < 0)
	{
		printf(1, "Error deallocating hoare slots monitor: #%d\n", hmonitor->id);
		return -1;
	}

	return 0;
}

int hoare_slots_monitor_addslots(hoare_slots_monitor_t *hmonitor, int n)
{
	// lock slot mutex (enter CS)
	if (kthread_mutex_lock(hmonitor->slotsMutex) < 0)
		return -1;

	// while there are no slots and stopAdding = 0, continue adding:
	while ( (hmonitor->stopAdding == 0) && (hmonitor->slots <= 0) )
	{
		// add more slots
		hmonitor->slots += n;

		// nofity waiting threads for slots, and pass slotsMutex lock
		// will wait for slotsMutex to be unlocked
		hoare_cond_signal(hmonitor->slotsHcond, hmonitor->slotsMutex);
		// ... will come back with slotsMutex locked

		// wait for signal if more slots are needed or stopAdding signal
		// release slotsMutex for now
		hoare_cond_wait(hmonitor->addingHcond, hmonitor->slotsMutex);
		// ...will come back with slotsMutex locked

		// release the lock so the signaler thread will continue
		kthread_mutex_unlock(hmonitor->slotsMutex);
		// lock the slotsMutex back to enter CS
		kthread_mutex_lock(hmonitor->slotsMutex);
	}

	// release slotsMutex (exit CS)
	if (kthread_mutex_unlock(hmonitor->slotsMutex) < 0)
		return -1;

	return 0;
}

int hoare_slots_monitor_takeslot(hoare_slots_monitor_t *hmonitor)
{
	// lock slot mutex (enter CS)
	if (kthread_mutex_lock(hmonitor->slotsMutex) < 0)
		return -1;

	// while there are no slots
	while (hmonitor->slots <= 0)
	{
		// wait on the hoare cond and release slotsMutex
		hoare_cond_wait(hmonitor->slotsHcond, hmonitor->slotsMutex);
		// ...will come back with slotsMutex locked

		// release the lock so the signaler thread will continue
		kthread_mutex_unlock(hmonitor->slotsMutex);
		// lock back the slotsMutex to enter CS
		kthread_mutex_lock(hmonitor->slotsMutex);
	}

	// take one
	hmonitor->slots--;

	// if there are more slots
	if (hmonitor->slots > 0)
	{
		// notify waiting thread on slots, and pass it the slotsMutex lock
		hoare_cond_signal(hmonitor->slotsHcond, hmonitor->slotsMutex);
	}
	else
	{
		// if the last slot was taken, pass slotMutex to thread waiting on addingHcond
		hoare_cond_signal(hmonitor->addingHcond, hmonitor->slotsMutex);
	}

	// release slotsMutex (exit CS)
	if (kthread_mutex_unlock(hmonitor->slotsMutex) < 0)
		return -1;

	return 0;
}

int hoare_slots_monitor_stopadding(hoare_slots_monitor_t *hmonitor)
{
	// lock slot mutex (enter CS)
	if (kthread_mutex_lock(hmonitor->slotsMutex) < 0)
		return -1;

	hmonitor->stopAdding = 1;
	// signal to stop adding
	// pass slot mutex lock to waiting thread on addingHcond
	if (hoare_cond_signal(hmonitor->addingHcond, hmonitor->slotsMutex) < 0)
		return -1;

	// release slotsMutex (exit CS)
	if (kthread_mutex_unlock(hmonitor->slotsMutex) < 0)
		return -1;

	return 0;
}

int hoare_slots_monitor_waitingCount(hoare_slots_monitor_t *hmonitor)
{
	return hoare_cond_waitingCount(hmonitor->slotsHcond);
}
