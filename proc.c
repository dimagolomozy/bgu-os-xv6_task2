#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "spinlock.h"
#include "proc.h"

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

void
pinit(void)
{
	struct proc *p;
	initlock(&ptable.lock, "ptable");

	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
	{
		initlock(&p->lock, "process");
	}
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  struct kthread *t;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
    	goto found;
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
  t = p->threads;
  t->proc = p;
  t->state = EMBRYO;
  t->tid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((t->kstack = kalloc()) == 0)
  {
		t->state = UNUSED;
		p->state = UNUSED;
		return 0;
  }
  sp = t->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *t->tf;
  t->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *t->context;
  t->context = (struct context*)sp;
  memset(t->context, 0, sizeof *t->context);
  t->context->eip = (uint)forkret;

  mycprintf("cpu:%d allocated tid #%d in pid: #%d\n",cpu->id, t->tid, p->pid);
  return p;
}


//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  struct kthread *t;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  t = p->threads;
  memset(t->tf, 0, sizeof(*t->tf));
  t->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  t->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  t->tf->es = t->tf->ds;
  t->tf->ss = t->tf->ds;
  t->tf->eflags = FL_IF;
  t->tf->esp = PGSIZE;
  t->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  safestrcpy(t->name, "initcode", sizeof(t->name));
  p->cwd = namei("/");

  changeThreadState(t, RUNNABLE);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
	struct proc *proc;
	uint sz;

	proc = kthread->proc;
	acquire(&proc->lock);
	sz = proc->sz;
	if(n > 0)
	{
		if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
			return -1;
	}
	else if(n < 0)
	{
		if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
			return -1;
	}

	proc->sz = sz;
	release(&proc->lock);
	switchuvm(kthread);
	return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;
  struct kthread *nt;
  struct proc *proc = kthread->proc;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  nt = np->threads;
  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(nt->kstack);

    nt->kstack = 0;
    nt->state = UNUSED;
    np->state = UNUSED;
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  *nt->tf = *kthread->tf;

  // Clear %eax so that fork returns 0 in the child.
  nt->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));
  safestrcpy(nt->name, kthread->name, sizeof(kthread->name));
 
  //return PROCESS pid, since we treat fork() the same as before.
  pid = np->pid;

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
  changeThreadState(nt, RUNNABLE);
  release(&ptable.lock);
  
  mycprintf("cpu:%d tid: #%d forked tid: #%d in pid: #%d\n",cpu->id, kthread->tid, nt->tid, np->pid);
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
	struct proc *p;
	struct proc *proc;
	struct kthread *t;
	int fd;

	proc = kthread->proc;
	if(proc == initproc)
	{
		panic("init exiting");
	}

	begin_op();
	iput(proc->cwd);
	end_op();
	proc->cwd = 0;

	acquire(&ptable.lock);

	// Parent might be sleeping in wait().
	wakeup1(proc->parent);
	mycprintf("cpu:%d tid: #%d wakes pid: #%d\n",cpu->id, kthread->tid, proc->parent->pid);

	// Pass abandoned children to init.
	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
	{
		if(p->parent == proc)
		{
			p->parent = initproc;
			if(p->state == ZOMBIE)
				wakeup1(initproc);
		}
	}

	proc->killed = 1;
	for (t = proc->threads; t < &proc->threads[NTHREAD]; t++)
	{
		if (t->state != RUNNING && t->state != UNUSED)
		{
			t->state = ZOMBIE;
		}
	}

	// current thread is now zombie
	mycprintf("cpu:%d tid: #%d exits\n",cpu->id, kthread->tid);
	changeThreadState(kthread, ZOMBIE);
	if (proc->state == ZOMBIE)
	{
		// Close all open files.
		for(fd = 0; fd < NOFILE; fd++)
		{
			if(proc->ofile[fd])
			{
				fileclose(proc->ofile[fd]);
				proc->ofile[fd] = 0;
			}
		}
	}
	// Jump into the scheduler, never to return.
	sched();
	panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
	struct proc *p;
	struct kthread *t;
	int havekids, pid;

	acquire(&ptable.lock);
	for(;;)
	{
		// Scan through table looking for zombie children.
		havekids = 0;
		for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
		{
			if(p->parent != kthread->proc)
				continue;

			havekids = 1;
			// if p->state is ZOMBIE, it means all his threads are ZOMBIE or UNUSED
			acquire(&p->lock);
			if(p->state == ZOMBIE)
			{
				// delete all processes threads
				for (t = p->threads; t < &p->threads[NTHREAD]; t++)
				{
					if (t->state == ZOMBIE)
					{
						mycprintf("cpu:%d tid: #%d wait_killed tid: #%d\n", cpu->id, kthread->tid, t->tid);
						kfree(t->kstack);
						t->kstack = 0;
						t->state = UNUSED;
						t->tid = 0;
						t->proc = 0;
						t->name[0] = 0;
						t->killed = 0;
					}
				}
				mycprintf("cpu:%d tid: #%d wait_killed pid: #%d\n", cpu->id, kthread->tid, p->pid);
				pid = p->pid;
				freevm(p->pgdir);
				p->state = UNUSED;
				p->pid = 0;
				p->parent = 0;
				p->name[0] = 0;
				p->killed = 0;
				release(&p->lock);
				release(&ptable.lock);
				return pid;
			}
			release(&p->lock);
		}

		// No point waiting if we don't have any children.
		if(!havekids || kthread->proc->killed)
		{
			release(&ptable.lock);
			return -1;
		}

		// Wait for children to exit.  (See wakeup1 call in proc_exit.)
		//mycprintf("tid: %d sleeps on pid: %d\n", thread->tid, thread->proc->pid);
		sleep(kthread->proc, &ptable.lock);  //DOC: wait-sleep
	}
	return 0; // will never get here.
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
	struct proc *p;
	struct kthread *t;

	for(;;)
	{
		// Enable interrupts on this processor.
		sti();

		// Loop over process table looking for process to run.
		acquire(&ptable.lock);
		for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
		{
			if ( (p->state != RUNNABLE) && (p->state != RUNNING) )
				continue;

			for (t = p->threads; t < &p->threads[NTHREAD]; t++)
			{

				if (t->state != RUNNABLE)
					continue;

				// Switch to chosen thread.  It is the thread's job
				// to release ptable.lock & p->lock and then reacquire it
				// before jumping back to us.
				kthread = t;
				switchuvm(kthread);
				changeThreadState(t, RUNNING);
				swtch(&cpu->scheduler, kthread->context);
				switchkvm();


				// Thread is done running for now.
				// It should have changed its t->state and p-state before coming back.
				kthread = 0;
			}
		}
		release(&ptable.lock);
	}
}

// Enter scheduler.  Must hold ptable.lock and threadLock
// and have changed proc->state.
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if (cpu->ncli != 1)
    panic("sched locks");
  if(kthread->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  swtch(&kthread->context, cpu->scheduler);
  cpu->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
	acquire(&ptable.lock);  //DOC: yieldlock
	changeThreadState(kthread, RUNNABLE);
	sched();
	release(&ptable.lock);
}


// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    initlog();
  }
  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
	if(kthread == 0)
		panic("sleep");

	if(lk == 0)
		panic("sleep without lk");

	// Must acquire ptable.lock in order to
	// change p->state+thread state and then call sched.
	// Once we hold ptable.lock, we can be
	// guaranteed that we won't miss any wakeup
	// (wakeup runs with ptable.lock locked),
	// so it's okay to release lk.
	if (lk != &ptable.lock)
	{
		acquire(&ptable.lock);  //DOC: sleeplock1
		release(lk);
	}

	// Go to sleep.
	kthread->chan = chan;
	changeThreadState(kthread, SLEEPING);
	sched();

	// Tidy up.
	kthread->chan = 0;

	// Reacquire original lock.
	if (lk != &ptable.lock)
	{
		release(&ptable.lock);  //DOC: sleeplock1
		acquire(lk);
	}
}

int changeProcState(struct proc *p)
{
	struct kthread *t;
	int states[7];
	int state;
	int i;

	// init the array with 0
	for (i = 0; i < 7; i ++)
	{
		states[i] = 0;
	}

	for (t = p->threads; t < &p->threads[NTHREAD]; t++)
	{
		states[t->state]++;
	}

	if (states[RUNNING] != 0)
	{
		state = RUNNING;
		goto end;
	}
	if (states[RUNNABLE] != 0)
	{
		state = RUNNABLE;
		goto end;
	}
	if (states[SLEEPING] != 0)
	{
		state = SLEEPING;
		goto end;
	}
	if (states[BLOCKED] != 0)
	{
		state = BLOCKED;
		goto end;
	}
	if (states[EMBRYO] != 0)
	{
		state = EMBRYO;
		goto end;
	}
	if (states[ZOMBIE] != 0)
	{
		state = ZOMBIE;
		goto end;
	}
	if (states[UNUSED] != 0)
	{
		state = UNUSED;
		goto end;
	}

	return -1; // ERROR

end:
	p->state = state;
	return state;
}

void changeThreadState(struct kthread *t, int state)
{
	t->state = state;
	acquire(&t->proc->lock);
	changeProcState(t->proc);
	//mycprintf("cpu:%d tid: #%d state is %d \n", cpu->id, t->tid, t->proc->state);
	release(&t->proc->lock);
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
void
wakeup1(void *chan)
{
	struct proc *p;
	struct kthread *t;

	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
	{
		for (t = p->threads; t < &p->threads[NTHREAD]; t++)
		{
			if(t->state == SLEEPING && t->chan == chan)
			{
				changeThreadState(t, RUNNABLE);
			}
		}
	}
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
	struct proc *p;
	struct kthread *t;

	acquire(&ptable.lock);
	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
	{
		if(p->pid == pid)
		{
			p->killed = 1;
			// Wake process from sleep if necessary.
			//acquire(&p->threadLock);
			for (t = p->threads; t < &p->threads[NTHREAD]; t++)
			{
				if(t->state == SLEEPING)
				{
					changeThreadState(t, RUNNABLE);
				}
			}
			release(&ptable.lock);
			return 0;
		}
	}
	release(&ptable.lock);
	return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->threads->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}



