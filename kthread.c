#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "spinlock.h"
#include "proc.h"
#include "kthread.h"

/*******************************************************
        The implementation of kthread KLT package
 *******************************************************/

/*  extern stuff from proc.c  */
extern struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

extern int nextpid;
extern void forkret(void);
extern void trapret(void);
/******************************/

struct {
	struct spinlock lock;
	kthread_mutex_t mutexs[MAX_MUTEXES];
} mtable;

void pushThreadToQueue(kthread_mutex_t*, struct kthread*);
struct kthread* popThreadFromQueue(kthread_mutex_t*);
int isWaiting(kthread_mutex_t *);

int kthread_create(void*(*start_func)(), void* stack, uint stack_size)
{
	struct proc *p;
	struct kthread *t;
	char *sp;

	if (stack_size > MAX_STACK_SIZE)
		panic("trying to create thread with stack size bigger then MAX_STACK_SIZE");

	p = kthread->proc;
	// since we are adding a thread to an existing process
	// hold the lock for the duration of the kthread_create
	acquire(&p->lock);
	for (t = p->threads; t < &p->threads[NTHREAD]; t++)
	{
		if(t->state == UNUSED)
			goto found;
	}
	mycprintf("cpu:%d kthread_create could not find UNUSED thread in pid: #%d\n", cpu->id, p->pid);
	release(&p->lock);
	return -1;

found:
	t->proc = p;
	t->state = EMBRYO;
	t->tid = nextpid++;
	release(&p->lock);

	// Allocate kernel stack.
	if((t->kstack = kalloc()) == 0)
	{
		mycprintf("cpu:%d kthread_create in tid: #%d kalloc failed\n", cpu->id, t->tid);
		changeThreadState(t, UNUSED);
		return -1;
	}
	sp = t->kstack + KSTACKSIZE;

	// Leave room for trap frame.
	sp -= sizeof *t->tf;
	t->tf = (struct trapframe*)sp;

	// Set up new context to start executing at forkret,
	// which returns to trapret.
	sp -= 4;
	*(uint*)sp = (uint)trapret;

	sp -= sizeof *t->context;
	t->context = (struct context*)sp;
	memset(t->context, 0, sizeof *t->context);
	t->context->eip = (uint)forkret;

	memset(t->tf, 0, sizeof(*t->tf));
	t->tf->cs = (SEG_UCODE << 3) | DPL_USER;
	t->tf->ds = (SEG_UDATA << 3) | DPL_USER;
	t->tf->es = t->tf->ds;
	t->tf->ss = t->tf->ds;
	t->tf->eflags = FL_IF;
	t->tf->esp = (uint)stack + stack_size;
	t->tf->eip = (int)start_func;  // beginning of start_func

	safestrcpy(t->name, "newthread", sizeof(t->name));

	changeThreadState(t, RUNNABLE);
	mycprintf("cpu:%d allocated tid: #%d in pid: #%d\n", cpu->id, t->tid, p->pid);
	return t->tid;
}

int kthread_id()
{
	if (kthread)
		return kthread->tid;

	return -1;
}

/*
 * kill only the current thread, and leave the parent process running.
 * unless you are the last one out.
 */
void
kthread_exit()
{
	struct proc *proc;

	mycprintf("cpu:%d tid: #%d kthread_exiting..\n", cpu->id, kthread->tid);
	proc = kthread->proc;

	acquire(&ptable.lock);

	// Parent might be sleeping in wait()
	// or other thread might be sleeping on me.
	mycprintf("cpu:%d tid: #%d waking up pid: #%d\n", cpu->id, kthread->tid, proc->pid);
	wakeup1(proc);

	changeThreadState(kthread, ZOMBIE);

	// Jump into the scheduler (with fucking ptable lock), never to return.
	mycprintf("cpu:%d tid: #%d is now zombie\n", cpu->id, kthread->tid);
	sched();
	panic("zombie exit");
}


int kthread_join(int thread_id)
{
	struct proc *p;
	struct kthread *t;

	acquire(&ptable.lock);
	// Scan through table looking for zombie children.
	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
	{
		for (t = p->threads; t < &p->threads[NTHREAD]; t++)
		{
			if (t->tid != thread_id)
				continue;

			goto found;
		}
	}

	//not found, must be done executing, exit.
	release(&ptable.lock);
	return 0;

found:

	for (;;)
	{
		// No point waiting if we are dead
		if(kthread->proc->killed || kthread->killed || t->state == UNUSED)
		{
			release(&ptable.lock);
			return -1;
		}

		// found - clear and exit
		if (t->state == ZOMBIE)
		{
			mycprintf("kthread_join killed tid: #%d\n", t->tid);
			kfree(t->kstack);
			t->kstack = 0;
			t->state = UNUSED;
			t->tid = 0;
			t->proc = 0;
			t->name[0] = 0;
			t->killed = 0;
			release(&ptable.lock);
			return 0;
		}

		// wait for thread to exit
		mycprintf("cpu:%d tid: #%d joined tid: #%d and sleeps on pid: #%d\n", cpu->id, kthread->tid, t->tid, t->proc->pid);
		sleep(t->proc, &ptable.lock);  //DOC: wait-sleep
		mycprintf("cpu:%d tid: #%d woke up\n", cpu->id, kthread->tid);
	}

	return 0; // will never get here.
}

void minit()
{
	initlock(&mtable.lock, "mtable");
}

int kthread_mutex_alloc(void)
{
	kthread_mutex_t *mutex;
	int i;

	acquire(&mtable.lock);
	for (i = 0; i < MAX_MUTEXES; i++)
	{
		if (mtable.mutexs[i].state == MUTEX_USED)
			continue;

		//found MUTEX_UNUSED
		mutex = &mtable.mutexs[i];
		mutex->id = i;
		mutex->locked = 0;
		mutex->state = MUTEX_USED;
		mutex->queueHead = 0;
		mutex->queueTail = 0;
		//memset(&mutexs[i].queue, 0, sizeof mutexs[i].queue);
		release(&mtable.lock);
		return mutex->id;	// mutex id
	}
	// not found
	release(&mtable.lock);
	return -1;
}

int kthread_mutex_dealloc(int mutex_id)
{
	kthread_mutex_t *mutex;

	// check if the mutex_id is out of range
	if ( (mutex_id < 0) || (mutex_id >= MAX_MUTEXES) )
		return -1;

	acquire(&mtable.lock);

	// get the mutex
	mutex = &mtable.mutexs[mutex_id];

	// if the mutex is UNUSED we return -1
	if (mutex->state == MUTEX_UNUSED)
		goto error;

	//If mutex is locked, return error
	if (mutex->locked == 1)
		goto error;

	if (isWaiting(mutex))
	{
		mycprintf("cpu:%d tid: #%d trying to unlock mutex: #%d with blocking processes", cpu->id, kthread->tid, mutex_id);
		goto error;
	}


	// mutex is unlocked and no waiting process - mark as UNUSED. alloc() will init all fields
	mutex->state = MUTEX_UNUSED;
	release(&mtable.lock);
	return 0;

error:
	release(&mtable.lock);
	return -1;
}

int kthread_mutex_lock(int mutex_id)
{
	kthread_mutex_t *mutex;

	// check if the mutex_id is out of range
	if ( (mutex_id < 0) || (mutex_id >= MAX_MUTEXES) )
		return -1;

	acquire(&mtable.lock);

	// get the mutex
	mutex = &mtable.mutexs[mutex_id];

	// if the mutex is UNUSED we return -1
	if (mutex->state == MUTEX_UNUSED)
		goto error;

	// The xchg is atomic.
	// It also serializes, so that reads after acquire are not
	// reordered before it.
	if (xchg(&mutex->locked, 1) != 0)
	{
		//failed to acquire. add thread queue
		mycprintf("cpu:%d tid: #%d going to queue on mutex #%d\n", cpu->id, kthread->tid, mutex_id);
		pushThreadToQueue(mutex, kthread);
		changeThreadState(kthread, BLOCKED);
		release(&mtable.lock);

		acquire(&ptable.lock);
		sched();
		release(&ptable.lock);

		acquire(&mtable.lock);
		mycprintf("cpu:%d tid: #%d back from queue on mutex #%d\n", cpu->id, kthread->tid, mutex_id);
	}

	//you got the lock, go to critical section. good luck..
	mycprintf("cpu:%d tid: #%d locked mutex #%d\n", cpu->id, kthread->tid, mutex_id);
	release(&mtable.lock);
	return 0;

error:
	release(&mtable.lock);
	return -1;
}

int kthread_mutex_unlock(int mutex_id)
{
	kthread_mutex_t *mutex;
	struct kthread *t;

	// check if the mutex_id is out of range
	if ( (mutex_id < 0) || (mutex_id >= MAX_MUTEXES) )
		return -1;

	acquire(&mtable.lock);

	// get the mutex
	mutex = &mtable.mutexs[mutex_id];

	// if the mutex is UNUSED we return -1
	if (mutex->state == MUTEX_UNUSED)
		goto error;

	if (mutex->locked == 0)
	{
		mycprintf("cpu:%d tid: #%d trying to unlock unlocked mutex #%d\n", cpu->id, kthread->tid, mutex_id);
		goto error;
	}

	if (!isWaiting(mutex))
	{
		// no one is waiting for the mutex. release and exit
		while (xchg(&mutex->locked, 0) != 0)
			;
		// little reset
		mutex->queueHead = 0;
		mutex->queueTail = 0;
		mycprintf("cpu:%d tid: #%d unlocked mutex #%d\n", cpu->id, kthread->tid, mutex_id);
		release(&mtable.lock);
		return 0;
	}

	// someone is waiting like a little bitch, so get him
	t = popThreadFromQueue(mutex);
	changeThreadState(t, RUNNABLE);

	mycprintf("cpu:%d tid: #%d unlocked mutex #%d\n", cpu->id, kthread->tid, mutex_id);
	release(&mtable.lock);
	return 0;
error:
	release(&mtable.lock);
	return -1;
}

/**
 * Must enter this function with mutex_id1 locked
 * will pass it to kthread that is waiting on mutex_id2
 * is no one is waiting, will release both locks
 */
int kthread_mutex_yieldlock(int mutex_id1, int mutex_id2)
{
	kthread_mutex_t *mutex1, *mutex2;
	struct kthread *t;

	// check if the mutex_id is out of range
	if ( (mutex_id1 < 0) || (mutex_id1 >= MAX_MUTEXES) || (mutex_id2 < 0) || (mutex_id2 >= MAX_MUTEXES) )
		return -1;

	acquire(&mtable.lock);

	// get the mutex
	mutex1 = &mtable.mutexs[mutex_id1];
	mutex2 = &mtable.mutexs[mutex_id2];

	// if the mutex is UNUSED we return -1
	if ( (mutex1->state == MUTEX_UNUSED) || (mutex2->state == MUTEX_UNUSED) )
		goto error;

	if(mutex1->locked == 0)
	{
		mycprintf("cpu:%d tid: #%d mutex1 #%d is not locked\n", cpu->id, kthread->tid, mutex_id1);
		goto error;
	}

	if (!isWaiting(mutex2))
	{
		// no one is waiting for mutex2. release and exit
		while (xchg(&mutex2->locked, 0) != 0)
			;
		// little reset
		mutex2->queueHead = 0;
		mutex2->queueTail = 0;

		if (!isWaiting(mutex1))
		{
			// no one is waiting for the mutex. release and exit
			while (xchg(&mutex1->locked, 0) != 0)
				;
			// little reset
			mutex1->queueHead = 0;
			mutex1->queueTail = 0;
		}
		else
		{
			// someone is waiting for mutex1. get him
			t = popThreadFromQueue(mutex1);
			changeThreadState(t, RUNNABLE);
		}

		release(&mtable.lock);
		return 0;
	}

	// someone is waiting for mutex2. get him
	t = popThreadFromQueue(mutex2);
	changeThreadState(t, RUNNABLE);

	mycprintf("cpu:%d tid: #%d yieldlock mutex #%d to mutex #%d\n", cpu->id, kthread->tid, mutex_id1, mutex_id2);
	release(&mtable.lock);
	return 0;

error:
	release(&mtable.lock);
	return -1;
}

struct kthread* popThreadFromQueue(kthread_mutex_t *mutex)
{
	struct kthread *t = mutex->queue[mutex->queueHead];

	// remove him from the queue
	mutex->queueHead++;
	if (mutex->queueHead == MAX_MUTEX_QUEUE)
		mutex->queueHead = 0;

	return t;
}

void pushThreadToQueue(kthread_mutex_t *mutex, struct kthread *t)
{
	//add thread to queue
	mutex->queue[mutex->queueTail] = t;

	mutex->queueTail++;
	if (mutex->queueTail == MAX_MUTEX_QUEUE)
		mutex->queueTail = 0;
}

int isWaiting(kthread_mutex_t *mutex)
{
	// if not equal, then someone is waiting in the queue
	return mutex->queueHead != mutex->queueTail;
}


