#include "types.h"
#include "stat.h"
#include "user.h"
#include "mesa_slots_monitor.h"

static int monitorID = 1;

mesa_slots_monitor_t* mesa_slots_monitor_alloc()
{
	mesa_slots_monitor_t *mmonitor = (mesa_slots_monitor_t*)malloc(sizeof(*mmonitor));
	if (mmonitor == 0)
		return 0;

	mmonitor->slotsMcond = mesa_cond_alloc();
	if (mmonitor->slotsMcond == 0)
		return 0;

	mmonitor->addingMcond = mesa_cond_alloc();
	if (mmonitor->addingMcond == 0)
		return 0;

	mmonitor->slotsMutex = kthread_mutex_alloc();
	if (mmonitor->slotsMutex < 0)
		return 0;

	mmonitor->slots = 0;
	mmonitor->stopAdding = 0;
	mmonitor->id = monitorID++;

	return mmonitor;
}

int mesa_slots_monitor_dealloc(mesa_slots_monitor_t *mmonitor)
{
	int chk = 0;

	// dealloc slots cond
	chk += mesa_cond_dealloc(mmonitor->slotsMcond);

	// dealloc adding cond
	chk += mesa_cond_dealloc(mmonitor->addingMcond);

	// dealloc slots mutex
	chk += kthread_mutex_dealloc(mmonitor->slotsMutex);

	free(mmonitor);

	if (chk < 0)
	{
		printf(1, "Error deallocating mesa slots monitor: #%d\n", mmonitor->id);
		return -1;
	}

	return 0;
}

int mesa_slots_monitor_addslots(mesa_slots_monitor_t *mmonitor, int n)
{
	// lock slot mutex (enter CS)
	if (kthread_mutex_lock(mmonitor->slotsMutex))
		return -1;

	// while there are no slots and stopAdding = 0, continue adding:
	while ( (mmonitor->stopAdding == 0) && (mmonitor->slots <= 0) )
	{
		// add more slots
		mmonitor->slots += n;

		// nofity waiting threads for slots
		mesa_cond_signal(mmonitor->slotsMcond);

		// wait for signal if more slots are needed or stopAdding
		// release slotsMutex for now
		mesa_cond_wait(mmonitor->addingMcond, mmonitor->slotsMutex);
		// ...will come back with slotsMutex locked
	}

	// release slots mutex (exit CS)
	if (kthread_mutex_unlock(mmonitor->slotsMutex))
		return -1;

	return 0;
}

int mesa_slots_monitor_takeslot(mesa_slots_monitor_t *mmonitor)
{
	// lock slotsMutex (enter CS)
	if (kthread_mutex_lock(mmonitor->slotsMutex))
		return -1;

	// while there are no slots
	while (mmonitor->slots <= 0)
	{
		// wait on the mesa cond and release slotsMutex
		mesa_cond_wait(mmonitor->slotsMcond, mmonitor->slotsMutex);
		// ...will come back with slotsMutex locked

	}

	// take one
	mmonitor->slots--;

	// if there are more slots
	if (mmonitor->slots > 0)
	{
		// notify waiting thread
		mesa_cond_signal(mmonitor->slotsMcond);
	}
	else
	{	// if the last slot was taken, notify to add more
		mesa_cond_signal(mmonitor->addingMcond);
	}

	// release slotsMutex (exit CS)
	if (kthread_mutex_unlock(mmonitor->slotsMutex))
		return -1;

	return 0;
}

int mesa_slots_monitor_stopadding(mesa_slots_monitor_t *mmonitor)
{
	// lock slotMutex (enter CS)
	if (kthread_mutex_lock(mmonitor->slotsMutex))
		return -1;

	mmonitor->stopAdding = 1;
	if (mesa_cond_signal(mmonitor->addingMcond))
		return -1;

	// release slotsMutex (exit CS)
	if (kthread_mutex_unlock(mmonitor->slotsMutex))
		return -1;

	return 0;
}

int mesa_slots_monitor_waitingCount(mesa_slots_monitor_t *mmonitor)
{
	return mesa_cond_waitingCount(mmonitor->slotsMcond);
}
