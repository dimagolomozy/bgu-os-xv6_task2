#include "types.h"
#include "stat.h"
#include "user.h"
#include "mesa_cond.h"

static int condID = 1;

mesa_cond_t* mesa_cond_alloc()
{
	mesa_cond_t *mcond;

	mcond = (mesa_cond_t*)malloc(sizeof(*mcond));
	if (mcond == 0)
		return 0;

	mcond->csMutex = kthread_mutex_alloc();
	mcond->queueMutex = kthread_mutex_alloc();
	mcond->count = 0;
	mcond->id = condID++;

	if ( (mcond->csMutex < 0) || (mcond->queueMutex < 0) )
		return 0;

	// lock the queue mutex so kthreads will wait in the queue
	if (kthread_mutex_lock(mcond->queueMutex) < 0)
		return 0;

	return mcond;
}

int mesa_cond_dealloc(mesa_cond_t *mcond)
{
	int chk = 0;

	if (mcond->count != 0)
	{
		printf(1, "Try to dealloc mesa cond with threads still in the queue\n");
		return -1;
	}

	// release the lock. (should be no waiting)
	chk += kthread_mutex_unlock(mcond->queueMutex);

	// dealloc queue Mutex
	chk += kthread_mutex_dealloc(mcond->queueMutex);

	// dealloc cs Mutex
	chk += kthread_mutex_dealloc(mcond->csMutex);

	// free mesa cond
	free(mcond);

	if (chk < 0)
	{
		printf(1, "Error deallocating mesa cond: #%d\n", mcond->id);
		return -1;
	}

	return 0;
}

/**
 * This function must be called with mutex_id locked
 */
int mesa_cond_wait(mesa_cond_t *mcond, int mutex_id)
{
	// enter CS
	kthread_mutex_lock(mcond->csMutex);

	// add 1 to waiting kthread counter
	mcond->count++;

	// Release mutex_id
	if (kthread_mutex_unlock(mutex_id) < 0)
	{
		printf(1, "mutex_id: #%d is not locked\n", mutex_id);
		mcond->count--;
		kthread_mutex_unlock(mcond->csMutex);
		return -1;
	}


	// exit CS
	kthread_mutex_unlock(mcond->csMutex);

	// put kthread in conds queue
	// will block the waiting kthread and call sched()
	kthread_mutex_lock(mcond->queueMutex);

	/** woke up: **/

	// acquire mutex_id back
	kthread_mutex_lock(mutex_id);

	return 0;
}

int mesa_cond_signal(mesa_cond_t *mcond)
{
	// enter CS
	kthread_mutex_lock(mcond->csMutex);

	// if we have waiting kthreads
	if (mcond->count != 0)
	{
		mcond->count--;
		// release the queueMutex lock,
		// the waiting kthread will get this lock and queueMutex will stay locked
		kthread_mutex_unlock(mcond->queueMutex);
	}

	// exit CS
	kthread_mutex_unlock(mcond->csMutex);

	return 0;
}

int mesa_cond_waitingCount(mesa_cond_t *mcond)
{
	return mcond->count;
}
